<?php
    require_once('animal.php');
    require_once('ape.php');
    require_once('frog.php');

    $sheep = new Animal('Shaun');
    echo "Name : " . $sheep->name . "<br>"; 
    echo "Leg : " . $sheep->legs . "<br>"; 
    echo "Cold Blooded : " . $sheep->cold_blooded . "<br><br>"; 

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok->name . "<br>";
    echo "Leg : " . $kodok->legs . "<br>";
    echo "Cold Blooded : " . $kodok->cold_blooded . "<br>"; 
    echo $kodok->jump("Hop Hop") . "<br><br>" ; 

    $sunggokong = new Ape("Kera Sakti");
    echo "Name : " . $sunggokong->name . "<br>"; 
    echo "Leg : " . $sunggokong->legs . "<br>"; 
    echo "Cold Blooded : " . $sunggokong->cold_blooded . "<br>"; 
    echo $sunggokong->yell("Auoooo");

?>